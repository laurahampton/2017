---
name: "Bruno Gon\xE7alves"
talks:
- Natural Language Processing from Scratch
---

Bruno Gonçalves is a Data Science fellow at NYU's Center for Data Science while on leave from a tenured faculty position at Aix-Marseille Université. He has a strong expertise in using large scale datasets for the analysis of human behavior.  By processing and analyzing large datasets from Twitter, Wikipedia, web access logs, and Yahoo! Meme he studied how we can observe both large scale and individual human behavior in an obtrusive and widespread manner. The main applications have been to the study of Computational Linguistics, Information Diffusion, Behavioral Change and Epidemic Spreading.

Noemi Derzsy is a postdoctoral research associate at Social Cognitive Network Academic Research Center at Rensselaer Polytechnic Institute and a NASA Datanaut. Holding a PhD in Physics and research background in Physics and Computer Science, my interests revolve around the study of complex systems through real-world data. As a NASA Datanaut she uses Network Science and Data Science techniques to tackle questions/problems about space and life on Earth using NASA's vast amount of data sets collected through telescopes, robots, spacecraft, wind tunnels, laboratories and the cameras of astronauts.