---
name: Ahmed Abdalla
talks:
- Creating a Bittorrent Client using Asyncio
---

I'm alum of the Recurse Center, a retreat space for programmers to pursue personal projects that will make them better programmers. I'm a former Software Engineer at Amazon. I started my career as an Electrical Engineer and transitioned to Software Engineering through many projects and startups.