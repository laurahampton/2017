---
name: Z. D. Smith
talks:
- 'Nim: A New Option for Optimizing Inner Loops'
---

Z. D. Smith is a programmer from New York City. He's currently a senior software engineer at [MakeSpace][mksp], where he's worked since 2014. Before that he was a member of the Winter 2014 batch at [Recurse Center][rc] (née Hacker School). 

[mksp]: http://www.makespace.com
[rc]: http://recurse.com