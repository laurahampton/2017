---
name: Dror Ayalon
talks:
- Music Synthesis in Python
---

My name is Dror Ayalon. I’m a maker, product manager, user experience and interaction designer, user interface designer, and a programmer.   I study for a masters degree at New York University’s Interactive Telecommunication Program (ITP).  

During my studies, I research and develop innovative music creation tools, using digital signal processing (DSP), music information retrieval (MIR) techniques, and machine learning algorithms, that will allow musicians to compose music in a variety of new ways and formats.  
  
I worked for 7 years in the start-up industry, most notably as the Product Manager for [Viber](https://www.viber.com/) ([acquired for $900M](https://techcrunch.com/2014/02/13/japanese-internet-giant-rakuten-acquires-viber-for-900m/)) and as the VP of Product Manager for Buynando Technologies.

For my BA I studied communications and majored in Interactive Media and Human-Computer Interactions (graduated cum laude).

I started to learn Computer Science and programming 4 years ago, and it quickly became my biggest interest and the focus of my research.
I developed and published a variety of Python based web applications, most of them can be seen on [my personal website](https://www.drorayalon.com) and on [GitHub](https://github.com/dodiku).  
  
Thanks a lot!  
Dror Ayalon