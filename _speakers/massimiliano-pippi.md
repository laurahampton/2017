---
name: Massimiliano Pippi
talks:
- A Python and a Gopher walk into a bar - Embedding Python in Go
---

Software developer for over 10 years, more than half spent working on scientific visualization and backend software for a private company, using C++ and Qt technologies. Then a lot of Python, Django and web related applications. I can ops. Open source advocate and active contributor, documentation fanatic, speaker at conferences (for the ❤ of sharing). I wrote a book once.

Currently at Datadog, where I'm allowed to play with high scalable systems, a wide number of different technologies, open source stuff, Python and Golang.