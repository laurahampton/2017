---
name: Matthew Rocklin
talks:
- Dask for task scheduling
---

Matthew is an open source software developer focusing on efficient computation and parallel computing, primarily within the Python ecosystem. He has contributed to many of the PyData libraries and today works on Dask a framework for parallel computing. Matthew holds a PhD in computer science from the University of Chicago where he focused on numerical linear algebra, task scheduling, and computer algebra.

Matthew lives in Brooklyn, NY and is employed by Continuum Analytics.