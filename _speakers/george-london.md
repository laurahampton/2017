---
name: George London
talks:
- 'import madness  # how to implement mergesort from scratch using only import statements'
---

George is a data engineer at Survata, a survey startup in sunny San Francisco. He's a self-taught programmer and Python was his 0th language. He likes philosophy, programming, epistemology, and finding ways to break them.