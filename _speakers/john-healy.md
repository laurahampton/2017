---
name: John Healy
talks:
- How to Identify, Train and Grow Junior Developers
---

I'm the Lead Engineer for LeafLink, a wholesale marketplace, CRM, and inventory management system for the cannabis industry. I also have over six years of software consulting experience as part of my agency, Atto, most of which has been on Python-based projects. I sometimes dream of optimizing the Django ORM these days.