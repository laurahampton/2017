---
name: Big Apple Py
tier: community
site_url: https://bigapplepy.org
logo: bap.png
---
Big Apple Py is a New York State non-profit that promotes the use and education
of open source software, in particular the Python programming language, in and
around New York City. Big Apple Py proudly organizes the
[NYC Python](http://nycpython.org) and
[Learn Python NYC](http://learn.nycpython.org) meetup groups, as well as
[PyGotham](https://pygotham.org), an annual regional Python conference.
